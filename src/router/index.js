import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Dashboard from '../views/dashboard/Dashboard.vue'
import Customers from '../views/dashboard/Customers.vue'
import Customer from '../views/dashboard/Customer.vue'
import AddCustomer from '../views/dashboard/AddCustomer.vue'
import EditCustomer from '../views/dashboard/EditCustomer.vue'
import EditTeam from '../views/dashboard/EditTeam.vue'
import Invoices from '../views/dashboard/Invoices.vue'
import Invoice from '../views/dashboard/Invoice.vue'
import AddInvoice from '../views/dashboard/AddInvoice.vue'
import SignUp from '../views/SignUp.vue'
import LogIn from '../views/Login.vue'
import MyAccount from '../views/dashboard/MyAccount.vue'

import store from '../store'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/sign-up',
    name: 'SignUp',
    component: SignUp
  },
  {
    path: '/log-in',
    name: 'LogIn',
    component: LogIn
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard,
    meta: {
      requireLogin: true
    }
  },
  {
    path: '/dashboard/Invoices',
    name: 'Invoices',
    component: Invoices,
    meta: {
      requireLogin: true
    }
  },
  {
    path: '/dashboard/Invoices/:id',
    name: 'Invoice',
    component: Invoice,
    meta: {
      requireLogin: true
    }
  },
  {
    path: '/dashboard/invoices/add',
    name: 'AddInvoice',
    component: AddInvoice,
    meta: {
      requireLogin: true
    }
  },
  {
    path: '/dashboard/customers',
    name: 'Customers',
    component: Customers,
    meta: {
      requireLogin: true
    }
  },
  {
    path: '/dashboard/customers/:id',
    name: 'Customer',
    component: Customer,
    meta: {
      requireLogin: true
    }
  },
  {
    path: '/dashboard/customers/add',
    name: 'AddCustomer',
    component: AddCustomer,
    meta: {
      requireLogin: true
    }
  },
  {
    path: '/dashboard/customers/:id/edit',
    name: 'EditCustomer',
    component: EditCustomer,
    meta: {
      requireLogin: true
    }
  },
  {
    path: '/dashboard/my-account',
    name: 'MyAccount',
    component: MyAccount,
    meta: {
      requireLogin: true
    }
  },
  {
    path: '/dashboard/my-account/edit-team',
    name: 'EditTeam',
    component: EditTeam,
    meta: {
      requireLogin: true
    }
  }
  
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requireLogin) && !store.state.isAuthenticated) {
    next('/log-in')
  } else {
    next()
  }
})

export default router
